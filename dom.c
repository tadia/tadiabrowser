#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "dom.h"

void appendChild(struct docNode* target, struct docNode* newChild) {
	if (target->child == NULL) {
		target->child = newChild;
	} else {
		struct docNode* lastChild = target->child;
		while (lastChild->next != NULL) {
			lastChild = lastChild->next;
		}
		
		lastChild->next = newChild;
		newChild->prev = lastChild;
	}
	
	newChild->parent = target;
}

void printNode(struct docNode* node, int depth) {
	printf("%*s", depth, "");
	
	switch (node->type) {
		case 0:
			puts("Document");
			break;
		default:
			printf("Text: %s\n", node->data);
			break;
	}
}

void printTree(struct docNode* node) {
	struct docNode* currentNode = node;
	int depth = 0;
	
	while (true) {
		printNode(currentNode, depth);
		
		if (currentNode->child) {
			currentNode = currentNode->child;
			depth++;
		} else if (currentNode->next) {
			currentNode = currentNode->next;
		} else {
			while (currentNode->next == NULL) {
				if (currentNode->parent) {
					currentNode = currentNode->parent;
					depth--;
				} else return;
			}
			currentNode = currentNode->next;
		}
	}
}

void freeNode(struct docNode* node) {
	free(node->data);
	free(node->meta);
	free(node);
}

void eraseTree(struct docNode* node) {
	struct docNode* currentNode = node;
	int depth = 0;
	
	while (true) {
		if (currentNode->child) {
			currentNode = currentNode->child;
			depth++;
		} else if (currentNode->next) {
			currentNode = currentNode->next;
		} else {
			while (currentNode->next == NULL) {
				struct docNode* tmpParent = currentNode->parent;
				freeNode(currentNode);

				if (tmpParent) {
					currentNode = tmpParent;
					depth--;
				} else return;
			}
			
			currentNode = currentNode->next;
			freeNode(currentNode->prev);
		}
	}
}
