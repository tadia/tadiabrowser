#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <uriparser/Uri.h>
#include <string.h>
#include "url.h"

struct redirectData* tb_redirect_data(int currentPort, char* currentHostname, char* currentURL, char* uriString) {
	struct redirectData* data = malloc(sizeof(struct redirectData));

	UriUriA uri;
	const char* errorPos;

	if (uriParseSingleUriA(&uri, uriString, &errorPos) != URI_SUCCESS) {
		return NULL;
	}

	int hostLen = uri.hostText.afterLast-uri.hostText.first;

	if (hostLen==0) {
		// Relative handling code
		UriUriA currentURI;
		UriUriA destURI;

		if (uriParseSingleUriA(&currentURI, currentURL, &errorPos) != URI_SUCCESS) {
			uriFreeUriMembersA(&uri);
			return NULL;
		}

		if (uriAddBaseUriA(&destURI, &uri, &currentURI) != URI_SUCCESS) {
			uriFreeUriMembersA(&uri);
			uriFreeUriMembersA(&destURI);
			return NULL;
		}

		char* destURIString;
		int charsRequired;

		if (uriToStringCharsRequiredA(&destURI, &charsRequired) != URI_SUCCESS) {
			uriFreeUriMembersA(&uri);
			uriFreeUriMembersA(&destURI);
			return NULL;
		}

		charsRequired++; // Extra NULL character

		destURIString = malloc(charsRequired * sizeof(char));
		if (destURIString == NULL) {
			uriFreeUriMembersA(&uri);
			uriFreeUriMembersA(&destURI);
			return NULL;
		}

		if (uriToStringA(destURIString, &destURI, charsRequired, NULL) != URI_SUCCESS) {
			uriFreeUriMembersA(&uri);
			uriFreeUriMembersA(&destURI);
			return NULL;
		}

		uriFreeUriMembersA(&destURI);

		data->url = destURIString;

		data->hostname = malloc(strlen(currentHostname)+1);
		strcpy(data->hostname, currentHostname);

		data->port = currentPort;
	} else {
		// Absolute handling code
		data->url = malloc(strlen(uriString)+1);
		strcpy(data->url, uriString);

		data->hostname = malloc(hostLen+1);
		memcpy(data->hostname, uri.hostText.first, hostLen);
		data->hostname[hostLen] = 0;

		char* portText = malloc(uri.portText.afterLast-uri.portText.first+1);
		memcpy(portText, uri.portText.first, uri.portText.afterLast-uri.portText.first);
		portText[uri.portText.afterLast-uri.portText.first] = 0;
		data->port = strtol(portText, NULL, 10);
		free(portText);

		data->port = data->port?data->port:1965;
	}

	//printf("data: %.*s\n", uri.pathTail->text.afterLast-uri.pathTail->text.first, uri.pathHead->text.first);

	uriFreeUriMembersA(&uri);

	return data;
}

void tb_redirect(struct docState* state, struct omniState* ostate, char* addr) {
	struct redirectData* data = tb_redirect_data(state->netstate->port, state->netstate->hostname, state->netstate->fullURL, addr);
	if (data) {
		ostate->selected = false;
		ostate->vcursor = -1;
		free(ostate->text);
		int urlLen = strlen(data->url);
		ostate->text = malloc(urlLen+1);
		ostate->text[urlLen] = 0;
		memcpy(ostate->text, data->url, urlLen);
		tb_netstate_free(state->netstate);
		state->netstate = tb_request(data->hostname, data->url, data->port);
		state->scroll = 0;
	}
}
