#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

const int bufferSize = 1024;

int tadia_gmiRequest(char* hostname, char* fullURL, char** responsePtr) {
	int sock = socket(AF_INET, SOCK_STREAM, 0); // Create socket
	if (sock == -1) return -1; // Fail state
	
	struct sockaddr_in req; // Define request parameters
	req.sin_addr.s_addr = inet_addr("127.0.0.1"); // Address: localhost
	req.sin_family = AF_INET; // IPv4
	req.sin_port = htons( 6666 ); // Port: 6666
	
	// Establish connection
	if (connect(sock, (struct sockaddr *) &req, sizeof(req)) < 0) return -2; // Fail state
	
	// Send data
	char* msg = "test";
	if (send(sock, msg, strlen(msg), 0) < 0) return -3;
	
	*responsePtr = hostname; // Write response to
	return 0;
};
