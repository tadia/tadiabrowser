#ifndef TADIA_REQUEST_H
#define TADIA_REQUEST_H

struct tadia_gmiConnection {
	// Input information
	char* hostname;
	char* fullURL;
	
	// State information
	int status;
		/*
		 * -3: internal error (TLS)
		 * -2: connection error
		 *  0: connection established; receiving data
		 * -1: invalid response
		 * ??: connection finished with ?? response code
		 * 20: data received and connection finished with response code 20
		 */
	struct tls_config* cfg;
	struct tls* ctx;
};

struct tadia_gmiConnection* tadia_gmiRequest(char* hostname, char* fullURL);

#endif
