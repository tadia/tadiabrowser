#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

void cmarch(int* cursor, char* input, bool invert) {
	while(*cursor < strlen(input) && ((input[*cursor] == ' ' || input[*cursor] == '\t') != invert)) (*cursor)++;
}

char *inputString(FILE* fp, size_t size){
//The size is extended by the input with the value of the provisional
    char *str;
    int ch;
    size_t len = 0;
    str = realloc(NULL, sizeof(*str)*size);//size is start size
    if(!str)return str;
    while(EOF!=(ch=fgetc(fp)) && ch != '\n'){
        str[len++]=ch;
        if(len==size){
            str = realloc(str, sizeof(*str)*(size+=16));
            if(!str)return str;
        }
    }
    str[len++]='\0';

    return realloc(str, sizeof(*str)*len);
}


void parse(char* input, bool* isRaw) {
	int cursor = 0;
	unsigned long len = strlen(input);
	if (strncmp(input, "```", 3) == 0) {
		int cursor = 3;
		cmarch(&cursor, input, false);
		if (len == cursor) {
			*isRaw = !(*isRaw);
			if (*isRaw) {
				printf("i'm a preformatting toggle line, the input is now raw\n");
			} else {
				printf("i'm a preformatting toggle line, the input is no longer raw\n");
			}
		} else {
			*isRaw = !(*isRaw);
			if (*isRaw) {
				printf("i'm a preformatting toggle line of type: %s, the input is now raw\n", input+cursor);
			} else {
				printf("i'm a preformatting toggle line of type: %s (why?), the input is no longer raw\n", input+cursor);
			}
		}
	} else if (*isRaw) {
		printf("i'm raw text: %s\n", input);
	} else {
		if (strncmp(input, "=>", 2) == 0) {
			int cursor = 2;
			cmarch(&cursor, input, false);
			int urlAt = cursor;
			cmarch(&cursor, input, true);
			int urlLen = cursor - urlAt;

			if (urlLen == 0) printf("bad link i am (text): %s\n", input);
			else {
				char* url = calloc(urlLen + 1, sizeof(char));
				strncpy(url, input+urlAt, urlLen);

				cmarch(&cursor, input, false);
				int textAt = cursor;
				cmarch(&cursor, input, true);
				int textLen = cursor - textAt;

				if (textLen == 0) {
					printf("i'm a link to %s\n", url);
				} else {
					printf("i'm a link to %s, named: %s\n", url, input+textAt);
				}

				free(url);
			}
		} else if (input[0] == '#') {
			int level = 1;
			if (input[1] == '#') {
				level++;
				if (input[2] == '#') {
					level++;
				}
			}

			int cursor = level;
			cmarch(&cursor, input, false);

			if (len - cursor == 0) printf("i'm a header of level %d, but i have no text\n", level);
			else printf("i'm a header of level %d: %s\n", level, input+cursor);
		} else if (input[0] == '*' && (input[1] == ' ' || input[1] == '\t' || len == 1)) {
			if (len > 2) printf("i'm a bullet point: %s\n", input+2);
			else printf("i'm an empty bullet point\n");
		} else if (input[0] == '>') {
			printf("i'm a quote: %s\n", input+((input[1]==' '||input[1]=='\t')?2:1));
		} else {
			printf("i'm text: %s\n", input);
		}
	}
}

int main() {
	bool isRaw = false;
	while (true) {
		char* input = inputString(stdin, 10);
		parse(input, &isRaw);
	}
}
