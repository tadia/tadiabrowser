#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/resource.h>
#include <string.h>

struct tadia_docNode {
	struct tadia_docNode* next; // Next child in linked list
	struct tadia_docNode* prev; // Previous child in linked list
	struct tadia_docNode* child; // Start of linked list of children
	struct tadia_docNode* parent; // Parent node
	int type;
	/*
	 * 0: Document
	 * 1: Plaintext
	 * 2: Link
	 * 3: Monospace
	 * 4: Heading
	 * 5: Quote
	 * 6: List
	 */
	char* data;
	char* meta;
};

void appendChild(struct tadia_docNode* target, struct tadia_docNode* newChild) {
	if (target->child == NULL) {
		target->child = newChild;
	} else {
		struct tadia_docNode* lastChild = target->child;
		while (lastChild->next != NULL) {
			lastChild = lastChild->next;
		}
		
		lastChild->next = newChild;
		newChild->prev = lastChild;
	}
	
	newChild->parent = target;
}

void printNode(struct tadia_docNode* node, int depth) {
	printf("%*s", depth, "");
	
	switch (node->type) {
		case 0:
			puts("Document");
			break;
		default:
			printf("Text: %s\n", node->data);
			break;
	}
}

void printTree(struct tadia_docNode* node) {
	struct tadia_docNode* currentNode = node;
	int depth = 0;
	
	while (true) {
		printNode(currentNode, depth);
		
		if (currentNode->child) {
			currentNode = currentNode->child;
			depth++;
		} else if (currentNode->next) {
			currentNode = currentNode->next;
		} else {
			while (currentNode->next == NULL) {
				if (currentNode->parent) {
					currentNode = currentNode->parent;
					depth--;
				} else return;
			}
			currentNode = currentNode->next;
		}
	}
}

void freeNode(struct tadia_docNode* node) {
	free(node->data);
	free(node->meta);
	free(node);
}

void eraseTree(struct tadia_docNode* node) {
	struct tadia_docNode* currentNode = node;
	int depth = 0;
	
	while (true) {
		if (currentNode->child) {
			currentNode = currentNode->child;
			depth++;
		} else if (currentNode->next) {
			currentNode = currentNode->next;
		} else {
			while (currentNode->next == NULL) {
				printf("removing node depth %d\n", depth);
				struct tadia_docNode* tmpParent = currentNode->parent;
				freeNode(currentNode);
				
				if (tmpParent) {
					currentNode = tmpParent;
					depth--;
				} else return;
			}
			
			printf("removing node depth %d\n", depth);
			currentNode = currentNode->next;
			freeNode(currentNode->prev);
		}
	}
}

int main() {
	/*struct tadia_docNode* docRoot = malloc(sizeof(struct tadia_docNode));
	struct tadia_docNode* docChildA = malloc(sizeof(struct tadia_docNode));
	struct tadia_docNode* docChildB = malloc(sizeof(struct tadia_docNode));
	appendChild(docRoot, docChildA);
	appendChild(docRoot, docChildB);
	 
	struct tadia_docNode* newChildA = malloc(sizeof(struct tadia_docNode));
	newChildA->type = 1;
	newChildA->data = (char*) malloc( 12 + 1 );
	strcpy(newChildA->data, "Hello world!");
	
	appendChild(docChildA, newChildA);
	
	struct tadia_docNode* newChildB = malloc(sizeof(struct tadia_docNode));
	newChildB->type = 1;
	newChildB->data = (char*) malloc( 11 + 1 );
	strcpy(newChildB->data, "Hello hodl!");
	
	appendChild(docChildB, newChildB);
	
	printTree(docRoot);
	eraseTree(docRoot);*/
	
	return 0;
}
