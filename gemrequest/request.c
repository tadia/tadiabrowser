#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <tls.h>
#include "request.h"

#define BUFFER_SIZE_RECEIVE 256

static void cleanup(struct tls *ctx, struct tls_config *cfg) {
	tls_close(ctx);
	tls_free(ctx);
	tls_config_free(cfg);
}

static int handle(char* data, int oldlen, int headerlen) {
	
}

int tadia_gmiRead(struct tadia_gmiConnection) {
}

struct tadia_gmiConnection* tadia_gmiRequest(char* hostname, char* fullURL) {
	struct tadia_gmiConnection* state = malloc(sizeof(struct tadia_gmiConnection)); // Create state object
	
	// Prepare TLS
	state->cfg = NULL;
	state->ctx = NULL;
	
	if (tls_init() != 0) { state->status=-3; return state; }
	if ((state->cfg = tls_config_new()) == NULL) { state->status=-3; return state; }
	
	// Config
	tls_config_insecure_noverifycert(state->cfg); // No certificate verification (gemini accepts self-signed certs)
	
	if ((state->ctx = tls_client()) == NULL) { state->status=-3; return state; }
	if (tls_configure(state->ctx, state->cfg) != 0) { cleanup(state->ctx, state->cfg); state->status=-3; return state; }
	
	// Connect to a server
	if (tls_connect(ctx, hostname, "1965") != 0) { cleanup(ctx, cfg); return -2; }

	// Send data
	int fullURLlen = strlen(fullURL);
	char* bufferSend = malloc(fullURLlen + 3);
	memcpy(bufferSend, fullURL, fullURLlen);
	memcpy(bufferSend + fullURLlen, "\r\n", 3);
	if (tls_write(ctx, bufferSend, strlen(bufferSend)) < 0) { cleanup(ctx, cfg); return -1; }
	free(bufferSend);

	// Get data
	char* buffer = calloc(BUFFER_SIZE_RECEIVE, sizeof(char));
	char* data = calloc(1, sizeof(char));
	
	int headerlen = 0;
	int rescode = 0;
	
	while (tls_read(ctx, buffer, BUFFER_SIZE_RECEIVE-1) != 0) {
		int oldlen = strlen(data);
		int bufferlen = strlen(buffer);
		
		data = realloc(data, (oldlen + bufferlen + 1) * sizeof(char));
		memcpy(data + oldlen, buffer, bufferlen + 1);
		
		if (headerlen == 0) {
			char* end = strstr(data, "\r\n");
			if (end != NULL) {
				headerlen = end - data;
				
				if (headerlen < 3) {
					cleanup(ctx, cfg);
					return -1;
				}
				
				// Parse header in a basic way
				if (data[0] >= 49 && data[0] <= 54) {
					rescode += ((int)data[0] - 48)*10;
					if (data[1] >= 48 && data[1] <= 57) {
						rescode += (int)data[1] - 48;
					}
					
					/* *metaPtr = malloc(headerlen - 2);
					strncpy(*metaPtr, data + 3, headerlen - 3);*/
					
					if (data[0] != '2') {
						cleanup(ctx, cfg);
						return rescode;
					}
					
					handle(data, oldlen, headerlen);
				} else {
					cleanup(ctx, cfg);
					return -1;
				}
			}
		} else {
			handle(data, oldlen, headerlen);
		}
		
		bzero(buffer, BUFFER_SIZE_RECEIVE);
	}
	
	// if (recv(sock, *responsePtr, 2000, 0) < 0) return 1; // No data
	
	cleanup(ctx, cfg);
	return rescode;
};
