#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

void nodeadd(int line, char* text, int len) {
	char buffer[128] = "";
	strncpy(buffer, text, len);
	printf("LINE %d: %s\n", line, buffer);
}

void handle(char* text, int* line, int* cursor) {
	while (true) {
		char* ptr = strstr(text, "\n");
		if (ptr == 0) {
			int textlen = strlen(text);
			nodeadd(*line, text, textlen);
			puts("no newline found");
			*cursor = textlen;
			break;
		} else {
			long int nlindex = ptr-text;
			nodeadd(*line, text, nlindex);
			text = ptr + 1;
			(*line)++;
		}
	}
}

int main() {
	int line = 0;
	int cursor = 0;
	handle("# Test of heading\n\nhello", &line, &cursor);
	handle(" world\n\n```\n#include <stdio.h>\nint main() {\n	puts(\"hello world!\");\n}", &line, &cursor);
	handle("\n```", &line, &cursor);
	//char* a = "# Project Gemini\n\n## Overview\n\nGemini is a new internet protocol which:\n\n* Is heavier than gopher\n* Is lighter than the web\n* Will not replace either\n* Strives for maximum power to weight ratio\n* Takes user privacy very seriously\n\n## Resources\n\n=> news/	Official Project Gemini news\n=> docs/	Gemini documentation\n=> software/	Gemini software\n=> servers/	Known Gemini servers\n=> https://lists.orbitalfox.eu/listinfo/gemini	Gemini mailing list (down due to hardware failure)\n=> gemini://gemini.conman.org/test/torture/	Gemini client torture test\n\n## Web proxies\n\n=> https://portal.mozz.us/?url=gemini%3A%2F%2Fgemini.circumlunar.space%2F&fmt=fixed	Gemini-to-web proxy service\n=> https://proxy.vulpes.one/gemini/gemini.circumlunar.space	Another Gemini-to-web proxy service\n\n## Search engine\n\n=> gemini://geminispace.info/	geminispace.info\n\n## Geminispace aggregators\n\n=> capcom/	CAPCOM\n=> gemini://rawtext.club:1965/~sloum/spacewalk.gmi	Spacewalk\n=> gemini://calcuode.com/gmisub-aggregate.gmi	gmisub\n=> gemini://caracolito.mooo.com/deriva/	Bot en deriva (Spanish language content)\n\n## Gemini mirrors of web resources\n\n=> gemini://gempaper.strangled.net/mirrorlist/	A list of mirrored services\n\n## Free Gemini hosting\n\n=> users/	Users with Gemini content on this server";
	/*char* b = "\n\r";
	
	char* end = strstr(a, b);
	printf("%ld", end-a);*/
}

