#include <stdlib.h>
#include <string.h>
#include <cairo/cairo.h>
#include <SDL2/SDL.h>

#include "history.h"
#include "window.h"

// Returns added node (if successful) or the node with the same parameters as the potential new one
struct historyNode* tb_history_add(struct historyNode* node, char* url) {
	// Check if this history branch already has same url
	for (int i = 0; i < node->childNum; i++) {
		if (strcmp(url, node->children[i]->url)) return node->children[i];
	}

	// Add new node to current history branch
	node->children = realloc(node->children, sizeof(struct historyNode*)*(++node->childNum));
	struct historyNode* newChild = malloc(sizeof(struct historyNode));

	newChild->url = malloc(strlen(url) + 1);
	strcpy(newChild->url, url);
	newChild->childNum = 0;
	newChild->children = malloc(0);
	newChild->parent = node;

	node->children[node->childNum] = newChild;
	return newChild;
}

void tb_history_draw(cairo_t* cr, struct historyState* hstate, SDL_Rect* destRect) {
	cairo_set_source_rgba(cr, 1, 1, 1, 1.0);
	cairo_rectangle(cr, 0, 0, destRect->w, destRect->h);
	cairo_fill(cr);
}
