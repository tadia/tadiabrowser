#include <stdbool.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tls.h>
#include "network.h"
#include "dom.h"

#define BUFFER_SIZE_RECEIVE 256

void cleanup(struct tb_request_state* state) {
	if (state->ctx != NULL) {
		tls_close(state->ctx);
		tls_free(state->ctx);
		state->ctx = NULL;
	}
	if (state->cfg != NULL) {
		tls_config_free(state->cfg);
		state->cfg = NULL;
	}
}

void network(struct tb_request_state* state) {
	// Prepare TLS
	state->cfg = NULL;
	state->ctx = NULL;

	if (tls_init() != 0) { state->status=-1; return; }
	if ((state->cfg = tls_config_new()) == NULL) { state->status=-1; return; }

	// Config
	tls_config_insecure_noverifycert(state->cfg); // No certificate verification (gemini accepts self-signed certs)

	if ((state->ctx = tls_client()) == NULL) { state->status=-1; return; }
	if (tls_configure(state->ctx, state->cfg) != 0) { cleanup(state); state->status=-1; return; }

	// Connect to a server
	if (tls_connect(state->ctx, state->hostname, "1965") != 0) { cleanup(state); state->status=-2; return; }

	state->status = 1; // Connection established; data sent

	// Send data
	int fullURLlen = strlen(state->fullURL);
	char* bufferSend = malloc(fullURLlen + 3);
	memcpy(bufferSend, state->fullURL, fullURLlen);
	memcpy(bufferSend + fullURLlen, "\r\n", 3);
	if (tls_write(state->ctx, bufferSend, strlen(bufferSend)) < 0) { cleanup(state); state->status=-3; return; }
	free(bufferSend);

	state->status = 2; // Data sent

	// Get data
	char* buffer = calloc(BUFFER_SIZE_RECEIVE, sizeof(char));
	state->data  = calloc(1, sizeof(char));

	while (tls_read(state->ctx, buffer, BUFFER_SIZE_RECEIVE-1) != 0) {
		state->status = 3; // Receiving data

		int oldlen = strlen(state->data);
		int bufferlen = strlen(buffer);

		state->data = realloc(state->data, (oldlen + bufferlen + 1) * sizeof(char));
		memcpy(state->data + oldlen, buffer, bufferlen + 1);

		bzero(buffer, BUFFER_SIZE_RECEIVE);
	}

	state->status = 4; // Received
}

void process(struct tb_request_state* state) {
	int headerlen = 0;

	char* end = strstr(state->data, "\r\n");
	if (end != NULL) {
		headerlen = end - state->data;

		if (headerlen < 3) {
			state->status = -6;
			return;
		}

		// Parse header in a basic way
		if (state->data[0] >= 49 && state->data[0] <= 54) {
			state->rescode += ((int)state->data[0] - 48)*10;
			if (state->data[1] >= 48 && state->data[1] <= 57) {
				state->rescode += (int)state->data[1] - 48;
			}

			/* *metaPtr = malloc(headerlen - 2);
			strncpy(*metaPtr, data + 3, headerlen - 3);*/
		} else {
			state->status = -6;
		}
	} else {
		state->status = -5;
	}

	state->text = end+2;
	state->headertext = (char*) calloc( headerlen - 2 /* -3 for ?? and space after and +1 for additional null terminator */, sizeof(char) );
	memcpy(state->headertext, state->data + 3, headerlen - 3);

	state->status = 5; // Processed
}

void cmarch(int* cursor, char* input, bool invert) {
	while(*cursor < strlen(input) && ((input[*cursor] == ' ' || input[*cursor] == '\t') != invert)) (*cursor)++;
}

void nodeadd(struct docNode* document, char* text, int len, bool* isRaw) {
	struct docNode* node = calloc(1, sizeof(struct docNode));

	char* buffer = (char*) calloc(len + 1, sizeof(char));
	memcpy(buffer, text, len);

	if (strncmp(buffer, "```", 3) == 0) {
		*isRaw = !(*isRaw);

		free(buffer);
		free(node);
	} else if (*isRaw) {
		node->data = buffer;
		node->type = 9;

		appendChild(document, node);
	} else {
		if (strncmp(buffer, "=>", 2) == 0) {
			int cursor = 2;
			cmarch(&cursor, buffer, false);
			int urlAt = cursor;
			cmarch(&cursor, buffer, true);
			int urlLen = cursor - urlAt;

			if (urlLen == 0) { // Invalid link (text)
				node->data = buffer;
				node->type = 1;
			} else {
				char* url = calloc(urlLen + 1, sizeof(char));
				strncpy(url, buffer+urlAt, urlLen);

				cmarch(&cursor, buffer, false);
				int textAt = cursor;
				cmarch(&cursor, buffer, true);
				int textLen = cursor - textAt;

				node->type = 2;
				node->meta = url;

				char* text = calloc(len - textAt + 1, sizeof(char));
				if (textLen == 0) { // URL-only link
					strncpy(text, buffer+urlAt, urlLen);
				} else { // Link with text
					strcpy(text, buffer+textAt);
				}
				node->data = text;
			}
		} else if (buffer[0] == '#') {
			int level = 1;
			if (buffer[1] == '#') {
				level++;
				if (buffer[2] == '#') {
					level++;
				}
			}

			node->type = 3+level;

			int cursor = level;
			cmarch(&cursor, buffer, false);

			char* text = calloc(len-cursor+1, sizeof(char));
			strcpy(text, buffer+cursor);
			node->data = text;
		} else if (buffer[0] == '*' && (buffer[1] == ' ' || buffer[1] == '\t' || len == 1)) {
			int offset = ((buffer[1]==' '||buffer[1]=='\t')?2:1);
			char* text = calloc(len-offset+1, sizeof(char));
			strcpy(text, buffer+offset);

			node->data = text;
			node->type = 8;
			free(buffer);
		} else if (buffer[0] == '>') {
			int offset = ((buffer[1]==' '||buffer[1]=='\t')?2:1);
			char* text = calloc(len-offset+1, sizeof(char));
			strcpy(text, buffer+offset);

			node->data = text;
			node->type = 7;
			free(buffer);
		} else {
			node->data = buffer;
			node->type = 1;
		}

		appendChild(document, node);
	}
}

void parse(struct tb_request_state* state) {
	bool isRaw = false;
	while (true) {
		char* ptr = strstr(state->text, "\n");
		if (ptr == 0) {
			nodeadd(state->document, state->text, strlen(state->text), &isRaw);
			break;
		} else {
			nodeadd(state->document, state->text, ptr-state->text, &isRaw);
			state->text = ptr + 1;
		}
	}
}

void* work(void* ptr) {
	struct tb_request_state* state = (struct tb_request_state*) ptr;

	while (true) {
		network(state);
		if (state->status != 4) break;
		process(state);
		if (state->status != 5 || state->rescode != 20) break;
		parse(state);
		if (state->status != 5 || state->rescode != 0) break;
		break;
	}

	state->finished = true;
	cleanup(state);
	return NULL;
}

void tb_netstate_free(struct tb_request_state* state) {
	if (state->finished) {
		free(state->data);
		free(state->headertext);
		free(state->hostname);
		free(state->fullURL);
		eraseTree(state->document);
		free(state);
	} else {
		state->deleted = true;
	}
}

struct tb_request_state* tb_request(char* hostname, char* fullURL, int port) {
	// Create state object
	struct tb_request_state* state = malloc(sizeof(struct tb_request_state));

	state->hostname = hostname;
	state->fullURL = fullURL;
	state->port = port;
	state->status = 0;
	state->finished = false;
	state->rescode = 0;
	state->document = calloc(1, sizeof(struct docNode));

	// Make a thread work on the state object
	pthread_create(&(state->worker), NULL, work, (void*) state);
	pthread_detach(state->worker);

	return state;
}


