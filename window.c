#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <cairo/cairo.h>
#include <pango/pangocairo.h>
#include <math.h>
#include <glib-2.0/glib-object.h>

#include "SDL_keycode.h"
#include "SDL_mouse.h"
#include "SDL_rect.h"
#include "dom.h"
#include "window.h"
#include "network.h"
#include "url.h"
#include "history.h"

#define WIN_WIDTH 640
#define WIN_HEIGHT 480
#define SCROLL_SPEED 60
#define MAX_FPS 120
#define OMNIBAR_HEIGHT 40
#define OMNIBAR_HEIGHT_PANGO OMNIBAR_HEIGHT*PANGO_SCALE

void drawRoundedRect(cairo_t* cr, double x, double y, double width, double height, double radius) {
	double degrees = M_PI / 180.0;
	cairo_new_sub_path (cr);
	cairo_arc (cr, x + width - radius, y + radius, radius, -90 * degrees, 0 * degrees);
	cairo_arc (cr, x + width - radius, y + height - radius, radius, 0 * degrees, 90 * degrees);
	cairo_arc (cr, x + radius, y + height - radius, radius, 90 * degrees, 180 * degrees);
	cairo_arc (cr, x + radius, y + radius, radius, 180 * degrees, 270 * degrees);
	cairo_close_path (cr);
}

static inline void drawDocument(cairo_t* cr, struct docState* state, SDL_Rect* destRect) {
	// Background fill
	if (state->netstate->finished) {
		switch (state->netstate->rescode) {
			case 20:
				cairo_set_source_rgba(cr, 1, 1, 1, 1.0);
				break;
			case 30:
			case 31:
				cairo_set_source_rgba(cr, 0.796, 0.674, 0.498, 1.0);
				break;
			default:
				cairo_set_source_rgba(cr, 0.796, 0.498, 0.498, 1.0);
		}
	} else {
		if (state->netstate->status < 0) cairo_set_source_rgba(cr, 0.796, 0.498, 0.498, 1.0);
		else cairo_set_source_rgba(cr, 0.761, 0.796, 0.498, 1.0);
	}
	cairo_rectangle(cr, 0, 0, destRect->w, destRect->h);
	cairo_fill(cr);

	if (state->netstate->finished) switch (state->netstate->rescode) {
		case 20:
			// Text rendering
			cairo_set_source_rgba(cr, 0, 0, 0, 1.0);

			if (state->netstate->document->child == NULL) return;
			struct docNode* document = state->netstate->document->child;

			int top = 16 * PANGO_SCALE - state->scroll;

			while (true) {
				PangoLayout* layout = pango_cairo_create_layout(cr);
				PangoFontDescription* desc = pango_font_description_new();
				PangoAttrList* list = pango_attr_list_new();

				if (document->data != NULL) pango_layout_set_text(layout, document->data, -1);

				pango_font_description_set_family(desc, (document->type == 9)?"Monospace":"Sans");
				if (document->type >= 4 && document->type <= 6) {
					pango_font_description_set_size(desc, (20+(6-document->type)*4)*PANGO_SCALE);
				} else pango_font_description_set_size(desc, 16*PANGO_SCALE);
				if (document->type == 2) {
					cairo_set_source_rgba(cr, 0, 0.38, 1, 1.0);
					PangoAttribute* underline = pango_attr_underline_new(PANGO_UNDERLINE_SINGLE);
					pango_attr_list_change(list, underline);
				} else {
					cairo_set_source_rgba(cr, 0, 0, 0, 1.0);
				}
				pango_layout_set_font_description(layout, desc);
				pango_layout_set_attributes(layout, list);
				pango_layout_set_width(layout, (destRect->w-(document->type==8?64:32))*PANGO_SCALE);

				pango_font_description_free(desc);
				pango_attr_list_unref(list);

				cairo_move_to(cr, 16+(document->type==8?32:0), top/PANGO_SCALE);

				int lheight;
				pango_layout_get_size(layout, NULL, &lheight);
				top += lheight; // + 16384; <-- add inter-paragraph spacing

				if (top > 0 && top-lheight < destRect->h * PANGO_SCALE) {
					int topPos = (top-lheight)/PANGO_SCALE;
					int bottomPos = top/PANGO_SCALE;

					// Render text
					pango_cairo_show_layout(cr, layout);

					// Render bullet point
					if (document->type == 8) {
						cairo_arc(cr, 24, topPos+(lheight/2/pango_layout_get_line_count(layout)/PANGO_SCALE), 4, 0, 2 * M_PI);
						cairo_set_source_rgb(cr, 0, 0, 0);
						cairo_fill(cr);
					}

					// Add link to clickables
					if (document->type == 2) {
						state->linkNum++;
						state->links = realloc(state->links, sizeof(struct linkPos) * state->linkNum);
						struct linkPos* pos = &(state->links[state->linkNum-1]);
						pos->start = topPos;
						pos->end = bottomPos;
						pos->addr = malloc(strlen(document->meta)+1);
						pos->startx = 0;
						pos->endx = destRect->w;
						strcpy(pos->addr, document->meta);
					}
				}

				g_object_unref(layout);

				if (document->next) {
					document = document->next;
				} else {
					break;
				}
			}

			state->docheight = top + state->scroll;

			// Render scrollbar
			if (state->docheight/PANGO_SCALE > destRect->h) {
				cairo_set_source_rgba(cr, 0, 0, 0, 0.2);
				cairo_rectangle(cr, destRect->w-6, destRect->h*state->scroll/state->docheight, 6, destRect->h*destRect->h*PANGO_SCALE/state->docheight);
				cairo_fill(cr);
			}
			break;
		case 30:
		case 31:
			// Text rendering for error text
			int lheight;
			{
				PangoLayout* layout = pango_cairo_create_layout(cr);

				// Set formatting
				PangoFontDescription* desc = pango_font_description_new();
				pango_font_description_set_size(desc, 24*PANGO_SCALE);
				pango_font_description_set_family(desc, "Sans");
				pango_layout_set_font_description(layout, desc);
				pango_font_description_free(desc);
				cairo_set_source_rgba(cr, 0, 0, 0, 1.0);

				// Set text
				char* originalText = "Dokument byl přesunut na jinou adresu: ";
				char* displayText = malloc(strlen(originalText) + strlen(state->netstate->headertext) + 1);
				strcpy(displayText, originalText);
				strcat(displayText, state->netstate->headertext);
				pango_layout_set_text(layout, displayText, -1);

				// Set position and size
				cairo_move_to(cr, 16, 16);
				pango_layout_set_width(layout, (destRect->w-32)*PANGO_SCALE);

				// Draw text
				pango_cairo_show_layout(cr, layout);

				// Get text size
				pango_layout_get_size(layout, NULL, &lheight);
				g_object_unref(layout);
			}

			// Text rendering for button text
			int bheight, bwidth;
			PangoLayout* layout = pango_cairo_create_layout(cr);

			// Set formatting
			PangoFontDescription* desc = pango_font_description_new();
			pango_font_description_set_size(desc, 20*PANGO_SCALE);
			pango_font_description_set_family(desc, "Sans");
			pango_layout_set_font_description(layout, desc);
			pango_font_description_free(desc);

			// Set text
			pango_layout_set_text(layout, "Přesměrovat", -1);

			// Get text size
			pango_layout_get_size(layout, &bwidth, &bheight);

			// Display redirect button
			drawRoundedRect(cr, 16, lheight / PANGO_SCALE + 32, bwidth / PANGO_SCALE + 24, bheight / PANGO_SCALE + 24, 12);
			cairo_set_source_rgb(cr, 0.408, 0.302, 0.153);
			cairo_fill_preserve(cr);

			// Set position and size
			cairo_move_to(cr, 28, lheight / PANGO_SCALE + 44);

			// Draw text
			cairo_set_source_rgba(cr, 1, 1, 1, 1.0);
			pango_cairo_show_layout(cr, layout);

			g_object_unref(layout);

			// Add button to clickables
			state->linkNum++;
			state->links = realloc(state->links, sizeof(struct linkPos) * state->linkNum);
			struct linkPos* pos = &(state->links[state->linkNum-1]);
			pos->start = lheight / PANGO_SCALE + 32;
			pos->end = (lheight + bheight) / PANGO_SCALE + 56;
			pos->addr = malloc(strlen(state->netstate->headertext)+1);
			pos->startx = 16;
			pos->endx = bwidth / PANGO_SCALE + 40;
			strcpy(pos->addr, state->netstate->headertext);
			break;
		default:
			{
				PangoLayout* layout = pango_cairo_create_layout(cr);

				// Set formatting
				PangoFontDescription* desc = pango_font_description_new();
				pango_font_description_set_size(desc, 24*PANGO_SCALE);
				pango_font_description_set_family(desc, "Sans");
				pango_layout_set_font_description(layout, desc);
				pango_font_description_free(desc);
				cairo_set_source_rgba(cr, 0, 0, 0, 1.0);

				// Set text
				switch (state->netstate->rescode) {
					case 10:
					case 11:
						pango_layout_set_text(layout, "Server požaduje zadání dat, ale tato funkce aktuálně není podporována", -1);
						break;
					case 40:
						pango_layout_set_text(layout, "Server hlásí dočasnou chybu bez dalších podrobností", -1);
						break;
					case 41:
						pango_layout_set_text(layout, "Server hlásí dočasnou chybu: Server je nedostupný kvůli přetížení nebo údržbě", -1);
						break;
					case 42:
						pango_layout_set_text(layout, "Server hlásí dočasnou chybu: Selhal systém na generování dynamického obsahu", -1);
						break;
					case 43:
						pango_layout_set_text(layout, "Server hlásí dočasnou chybu: Proxy server se nedokáže připojit k cíli", -1);
						break;
					case 50:
						pango_layout_set_text(layout, "Server hlásí trvalou chybu bez dalších podrobností", -1);
						break;
					case 51:
						pango_layout_set_text(layout, "Server hlásí trvalou chybu: Požadovaný dokument nebyl nalezen", -1);
						break;
					case 52:
						pango_layout_set_text(layout, "Server hlásí trvalou chybu: Požadovaný dokument byl odstraněn", -1);
						break;
					case 53:
						pango_layout_set_text(layout, "Server hlásí trvalou chybu: Tento server neakceptuje požadavky na dokumenty na jiných serverech (není to proxy server)", -1);
						break;
					case 59:
						pango_layout_set_text(layout, "Server hlásí trvalou chybu: Špatný požadavek od klienta", -1);
						break;
					case 60:
					case 61:
					case 62:
						pango_layout_set_text(layout, "Server požaduje klientský certifikát, ale tato funkce aktuálně není podporována", -1);
						break;
					default:
						char* originalText = "Server je dostupný, ale hlásí nepodporovaný stavový kód: ";
						char str[3];
						sprintf(str, "%d", state->netstate->rescode);
						char* displayText = malloc(strlen(originalText) + strlen(str) + 1);
						strcpy(displayText, originalText);
						strcat(displayText, str);
						pango_layout_set_text(layout, displayText, -1);
				}

				// Set position and size
				cairo_move_to(cr, 16, 16);
				pango_layout_set_width(layout, (destRect->w-32)*PANGO_SCALE);

				// Draw text
				pango_cairo_show_layout(cr, layout);
				g_object_unref(layout);
				break;
			}
	}
}

static inline void drawOmnibar(cairo_t* cr, struct omniState* ostate, SDL_Rect* destRect) {
	// Background fill for address part
	if (ostate->selected) cairo_set_source_rgba(cr, 0.4, 0.4, 0.4, 1.0);
	else cairo_set_source_rgba(cr, 0, 0, 0, 1.0);
	cairo_rectangle(cr, 0, 0, destRect->w-40, destRect->h);
	cairo_fill(cr);

	// Display address
	PangoLayout* layout = pango_cairo_create_layout(cr);

	// Set formatting
	PangoFontDescription* desc = pango_font_description_new();
	pango_font_description_set_size(desc, 16*PANGO_SCALE);
	pango_font_description_set_family(desc, "Sans");
	pango_layout_set_font_description(layout, desc);
	pango_font_description_free(desc);
	cairo_set_source_rgba(cr, 1, 1, 1, 1.0);

	// Set text
	pango_layout_set_text(layout, ostate->text, -1);

	// Set position
	int lheight;
	pango_layout_get_size(layout, NULL, &lheight);
	int offset = (OMNIBAR_HEIGHT_PANGO-lheight)/2/PANGO_SCALE;
	cairo_move_to(cr, offset, offset);

	// Update cursor postition on mouse change
	if (ostate->clickx != -1) {
		int mousex = (ostate->clickx-offset)*PANGO_SCALE;
		if (mousex < 0) {
			ostate->cursor = 0;
			ostate->ocursor = 0;
			ostate->vcursor = 0;
		} else {
			int endx = -1;
			bool change = false;
			PangoLayoutIter* iter = pango_layout_get_iter(layout);
			while (true) {
				int pendx = endx;
				int cursor = pango_layout_iter_get_index(iter);
				PangoRectangle crect;
				pango_layout_iter_get_char_extents(iter, &crect);
				endx = crect.x + crect.width;
				if (mousex >= crect.x && mousex < endx) {
					if (mousex >= (crect.x + endx) / 2) {
						ostate->cursor = cursor+1;
						ostate->ocursor = ostate->cursor;
						ostate->vcursor = endx/PANGO_SCALE;
					} else {
						ostate->cursor = cursor;
						ostate->ocursor = ostate->cursor;
						ostate->vcursor = crect.x/PANGO_SCALE;
					}
					change = true;
				}
				if (pendx > -1 && mousex >= pendx && mousex < crect.x) {
					ostate->cursor = cursor;
					ostate->ocursor = ostate->cursor;
					ostate->vcursor = crect.x/PANGO_SCALE;
					change = true;
				}
				if (!pango_layout_iter_next_char(iter)) {
					if (!change) {
						ostate->cursor = cursor + 1;
						ostate->ocursor = ostate->cursor;
						ostate->vcursor = endx/PANGO_SCALE;
					}
					break;
				}
			}
		}

		ostate->clickx = -1;
		ostate->clicky = -1;
	}

	// Update cursor position on external change
	if (ostate->cursor != ostate->ocursor) {
		bool change = false;
		PangoLayoutIter* iter = pango_layout_get_iter(layout);
		while (true) {
			int cursor = pango_layout_iter_get_index(iter);
			PangoRectangle crect;
			pango_layout_iter_get_char_extents(iter, &crect);

			if (cursor == ostate->cursor) {
				ostate->vcursor = crect.x/PANGO_SCALE;
				change = true;
			}

			if (!pango_layout_iter_next_char(iter)) {
				if (!change) {
					ostate->vcursor = (crect.x+crect.width)/PANGO_SCALE;
				}
				break;
			}
		}
		ostate->ocursor = ostate->cursor;
	}

	// Draw text
	pango_cairo_show_layout(cr, layout);

	// Draw cursor
	if (ostate->cursor > -1) {
		cairo_set_source_rgba(cr, 1, 1, 1, 1.0);
		cairo_rectangle(cr, offset+ostate->vcursor-1, offset, 2, lheight/PANGO_SCALE);
		cairo_fill(cr);
	}

	g_object_unref(layout);

	// Draw icon
	// Background fill for icon part
	cairo_set_source_rgba(cr, 0.364, 0.403, 0.126, 1.0);
	cairo_rectangle(cr, destRect->w-40, 0, 40, destRect->h);
	cairo_fill(cr);

	// Draw circles
	cairo_set_source_rgb(cr, 1, 1, 1);
	cairo_arc(cr, destRect->w-26, 7, 3, 0, 2 * M_PI);
	cairo_fill(cr);
	cairo_arc(cr, destRect->w-14, 7, 3, 0, 2 * M_PI);
	cairo_fill(cr);
	cairo_arc(cr, destRect->w-14, 33, 3, 0, 2 * M_PI);
	cairo_fill(cr);

	// Draw main body
	cairo_move_to(cr, destRect->w-26, 13);
	cairo_line_to(cr, destRect->w-26, 19);
	cairo_line_to(cr, destRect->w-14, 19);
	cairo_set_line_width(cr, 4);
	cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
	cairo_stroke(cr);

	cairo_move_to(cr, destRect->w-14, 13);
	cairo_line_to(cr, destRect->w-14, 27);
	cairo_stroke(cr);

}

void paint(struct docState* state, struct omniState* ostate, struct historyState* hstate) {
	// SDL_Rect for document drawing
	struct SDL_Rect* destRect = state->destRect;
	destRect->x=0;
	destRect->y=OMNIBAR_HEIGHT;
	destRect->w=state->width;
	destRect->h=state->height-OMNIBAR_HEIGHT;

	// SDL_Rect for omnibar drawing
	struct SDL_Rect omniRect;
	omniRect.x=0;
	omniRect.y=0;
	omniRect.w=state->width;
	omniRect.h=OMNIBAR_HEIGHT;

	void* pixels; int pitch;

	// Draw document
	SDL_Texture* texture = SDL_CreateTexture(state->renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, destRect->w, destRect->h);
	SDL_LockTexture(texture, NULL, &pixels, &pitch);
	cairo_surface_t* cairo_surface = cairo_image_surface_create_for_data(pixels, CAIRO_FORMAT_ARGB32, destRect->w, destRect->h, pitch);
	cairo_t* cr = cairo_create(cairo_surface);
	if (hstate->isShown) tb_history_draw(cr, hstate, destRect);
	else drawDocument(cr, state, destRect);
	SDL_UnlockTexture(texture);
	SDL_RenderCopy(state->renderer, texture, NULL, destRect);

	// Draw omnibar
	SDL_Texture* omni_texture = SDL_CreateTexture(state->renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, omniRect.w, omniRect.h);
	SDL_LockTexture(omni_texture, NULL, &pixels, &pitch);
	cairo_surface_t* omni_cairo_surface = cairo_image_surface_create_for_data(pixels, CAIRO_FORMAT_ARGB32, omniRect.w, omniRect.h, pitch);
	cairo_t* omni_cr = cairo_create(omni_cairo_surface);
	drawOmnibar(omni_cr, ostate, &omniRect);
	SDL_UnlockTexture(omni_texture);
	SDL_RenderCopy(state->renderer, omni_texture, NULL, &omniRect);

	SDL_RenderPresent(state->renderer);
	SDL_DestroyTexture(texture);
}

void scrollBy(int amount, struct docState* state) {
	state->scroll -= amount*PANGO_SCALE;
	
	// Cap scroll + (can produce negative results if height is higher, thus the negative cap function must be afterwards)
	if (state->scroll > state->docheight - state->destRect->h * PANGO_SCALE) state->scroll = state->docheight - state->destRect->h * PANGO_SCALE;
	
	// Cap scroll - (so it isn't negative)
	if (state->scroll < 0) state->scroll = 0;
}

int tb_window() {
	SDL_SetHint(SDL_HINT_VIDEODRIVER, "wayland,x11");
	SDL_Init(SDL_INIT_VIDEO);
	SDL_Window* window = SDL_CreateWindow("tadiabrowser", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIN_WIDTH, WIN_HEIGHT, SDL_WINDOW_RESIZABLE);
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	
	// Document state
	struct docState* state = calloc(1, sizeof(struct docState));
	state->renderer = renderer;
	state->destRect = malloc(sizeof(struct SDL_Rect));
	// In pixels
	state->width = WIN_WIDTH;
	state->height = WIN_HEIGHT;
	// In pango units
	state->docheight = -1;
	state->scroll = 0;
	state->linkNum = 0;
	state->links = NULL;

	// Omnibar state
	struct omniState* ostate = malloc(sizeof(struct omniState));
	ostate->selected = false;
	ostate->clickx = -1;
	ostate->clicky = -1;
	ostate->cursor = -1;
	ostate->ocursor = -1;
	ostate->vcursor = -1;

	// History state
	struct historyState* hstate = malloc(sizeof(struct historyState));
	hstate->isShown = false;

	// Cursor helpers
	struct SDL_Cursor* arrowCursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
	struct SDL_Cursor* handCursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND);
	struct SDL_Cursor* textCursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_IBEAM);

	// Request website
	char* originalHostStack = "gemini.circumlunar.space";
	char* originalHost = malloc(strlen(originalHostStack) + 1);
	strcpy(originalHost, originalHostStack);
	char* originalURLStack = "gemini://gemini.circumlunar.space/";
	char* originalURL = malloc(strlen(originalURLStack) + 1);
	strcpy(originalURL, originalURLStack);
	state->netstate = tb_request(originalHost, originalURL, 1965);
	bool oldfinished = false;

	ostate->text = malloc(strlen(originalURL) + 1);
	strcpy(ostate->text, originalURL);

	paint(state, ostate, hstate);
	
	bool done = false;
	while (!done) {
		bool repaint = false;
		
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
				case SDL_QUIT:
					done = true;
					break;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym) {
						case SDLK_ESCAPE:
							if (ostate->selected) {
								ostate->selected = false;
								ostate->cursor = -1;
								ostate->vcursor = -1;
								repaint = true;
							} else done = true;
							break;
						case SDLK_LEFT:
							if (!ostate->selected) break;
							ostate->cursor--;
							repaint = true;
							if (ostate->cursor < 0) ostate->cursor = 0;
							break;
						case SDLK_RIGHT:
							if (!ostate->selected) break;
							ostate->cursor++;
							repaint = true;
							if (ostate->cursor > strlen(ostate->text)) ostate->cursor = strlen(ostate->text);
							break;
						case SDLK_BACKSPACE:
							if (ostate->selected && ostate->cursor > 0) {
								int textLen = strlen(ostate->text);
								char* newText = malloc(textLen);
								memcpy(newText, ostate->text, ostate->cursor-1);
								memcpy(newText+ostate->cursor-1, ostate->text+ostate->cursor, textLen-ostate->cursor);
								newText[textLen-1] = 0;
								free(ostate->text);
								ostate->text = newText;
								ostate->cursor--;
								repaint = true;
							}
							break;
						case SDLK_RETURN:
							if (ostate->selected) tb_redirect(state, ostate, ostate->text);
							break;
					}
					break;
				case SDL_TEXTINPUT:
					if (!ostate->selected) break;
                    //strcat(ostate->text, event.text.text);
					int textLen = strlen(ostate->text);
					int addLen = strlen(event.text.text);
					if (addLen != 1) break;
					char* newText = malloc(textLen + addLen + 1);
					memcpy(newText, ostate->text, ostate->cursor);
					memcpy(newText+ostate->cursor, event.text.text, addLen);
					memcpy(newText+ostate->cursor+addLen, ostate->text+ostate->cursor, textLen-ostate->cursor);
					newText[textLen + addLen] = 0;
					free(ostate->text);
					ostate->text = newText;
					ostate->cursor += addLen;
					repaint = true;
                    break;
				case SDL_WINDOWEVENT:
					switch (event.window.event) {
						//Get new dimensions and repaint on window size change
						case SDL_WINDOWEVENT_SIZE_CHANGED:
							state->width = event.window.data1;
							state->height = event.window.data2;
							scrollBy(0, state);
						case SDL_WINDOWEVENT_EXPOSED:
							repaint = true;
							break;
					}
					break;
				case SDL_MOUSEMOTION:
					if (event.button.y < OMNIBAR_HEIGHT) {
						if (event.button.x < state->width - 40) SDL_SetCursor(textCursor);
						else SDL_SetCursor(handCursor);
					} else {
						bool cursorIsHand = false;
						for (int i = 0; i < state->linkNum; i++) {
							struct linkPos* link = &state->links[i];
							if (link->start + state->destRect->y <= event.button.y && link->end + state->destRect->y > event.button.y && link->startx + state->destRect->x <= event.button.x && link->endx + state->destRect->x > event.button.x) cursorIsHand = true;
						}
						SDL_SetCursor(cursorIsHand?handCursor:arrowCursor);
					}
					break;
				case SDL_MOUSEWHEEL:
					if (event.wheel.mouseY >= OMNIBAR_HEIGHT && !hstate->isShown) scrollBy(event.wheel.y*SCROLL_SPEED, state);
					repaint = true;
					break;
				case SDL_MOUSEBUTTONDOWN:
					if (event.button.y < OMNIBAR_HEIGHT && event.button.x < state->width - 40) {
						ostate->selected = true;
						ostate->clickx = event.button.x;
						ostate->clicky = event.button.y;
					} else {
						ostate->selected = false;
						ostate->cursor = -1;
						ostate->vcursor = -1;
					}

					if (event.button.y < OMNIBAR_HEIGHT && event.button.x >= state->width - 40) {
						hstate->isShown = !hstate->isShown;
					}
					repaint = true;
				case SDL_MOUSEBUTTONUP:
					if (event.button.y >= OMNIBAR_HEIGHT) {
						for (int i = 0; i < state->linkNum; i++) {
							struct linkPos* link = &state->links[i];
							if (link->start + state->destRect->y <= event.button.y && link->end + state->destRect->y > event.button.y && link->startx + state->destRect->x <= event.button.x && link->endx + state->destRect->x > event.button.x) {
								tb_redirect(state, ostate, link->addr);
							}
						}
					}
				default:
					break;
			}
		}

		if (state->netstate->finished != oldfinished) repaint=true;
		oldfinished = state->netstate->finished;
		
		if (repaint) {
			// Clear link data
			for (int i = 0; i < state->linkNum; i++) free(state->links[i].addr);
			free(state->links);
			state->linkNum = 0;
			state->links = NULL;

			paint(state, ostate, hstate);
		}
	}
	
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
