#include <stdio.h>
#include <stdlib.h>
#include "url.h"

int main() {
	struct redirectData* data = tb_redirect(1965, "gemini.circumlunar.space", "docs/specification.gmi");
	if (data) {
		printf(" URL: %s\n", data->url);
		printf("host: %s\n", data->hostname);
		printf("port: %d\n", data->port);
	} else {
		printf("error");
	}

	return 0;
}
