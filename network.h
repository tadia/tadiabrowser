#ifndef TB_NETWORK_H
#define TB_NETWORK_H

#include <pthread.h>
#include <stdbool.h>

struct tb_request_state {
	// Input information
	char* hostname;
	char* fullURL;
	int port;

	// State information
	int status;
	/*
	 *  0: waiting for connection
	 * -1: internal error (TLS)
	 *  1: connection established
	 * -2: connection error
	 *  2: data sent
	 * -3: failure while sending data
	 *  3: receiving data
	 *  4: data received
	 * -4: timeout
	 *  5: data processed
	 * -5: invalid data
	 * -6: invalid response code
	 */
	bool finished;
	bool deleted;
	struct tls_config* cfg;
	struct tls* ctx;
	char* data;
	pthread_t worker;

	// Output information
	char* text;
	char* headertext;
	int rescode;
	struct docNode* document;
};

struct tb_request_state* tb_request(char* hostname, char* fullURL, int port);
void tb_netstate_free(struct tb_request_state* state);

#endif
