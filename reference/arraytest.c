#include <stdlib.h>
#include <stdio.h>

struct linkPos {
	int start;
	int end;
	char* addr;
};

int main() {
	int linkNum = 0;
	struct linkPos* pos = NULL;
	for (int i = 0; i < 5; i++) {
		linkNum++;
		pos = realloc(pos, sizeof(struct linkPos) * linkNum);
		pos[linkNum-1].start = 1;
		pos[linkNum-1].end = -8;
		pos[linkNum-1].addr = malloc(sizeof(char) * 6);
		pos[linkNum-1].addr[0] = 'h';
		pos[linkNum-1].addr[1] = 'e';
		pos[linkNum-1].addr[2] = 'l';
		pos[linkNum-1].addr[3] = 'l';
		pos[linkNum-1].addr[4] = 'o';
		pos[linkNum-1].addr[5] = 0;

		linkNum++;
		pos = realloc(pos, sizeof(struct linkPos) * linkNum);
		pos[linkNum-1].start = 5;
		pos[linkNum-1].end = 99;
		pos[linkNum-1].addr = malloc(sizeof(char) * 4);
		pos[linkNum-1].addr[0] = 'b';
		pos[linkNum-1].addr[1] = 'y';
		pos[linkNum-1].addr[2] = 'e';
		pos[linkNum-1].addr[3] = 0;

		for (int i = 0; i < linkNum; i++) {
			printf("%d, %d, %s\n", pos[i].start, pos[i].end, pos[i].addr);
		}

		for (int i = 0; i < linkNum; i++) free(pos[i].addr);
		free(pos);
		pos = NULL;
		linkNum = 0;
	}

	return 0;
}
