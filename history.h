#ifndef TB_HISTORY_H
#define TB_HISTORY_H

#include <stdbool.h>
#include <cairo/cairo.h>
#include <SDL2/SDL.h>

struct historyNode {
	char* url;
	struct historyNode* parent;
	struct historyNode** children;
	int childNum;
};

struct historyState {
	bool isShown;
};

void tb_history_draw(cairo_t* cr, struct historyState* hstate, SDL_Rect* destRect);

#endif
