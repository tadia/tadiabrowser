#ifndef TB_DOM_H
#define TB_DOM_H

struct docNode {
	struct docNode* next; // Next child in linked list
	struct docNode* prev; // Previous child in linked list
	struct docNode* child; // Start of linked list of children
	struct docNode* parent; // Parent node
	int type;
	/*
	 * 0: Document
	 * 1: Plaintext
	 * 2: Link
	 * 3: Monospace
	 * 4: Heading 1
	 * 5: Heading 2
	 * 6: Heading 3
	 * 7: Quote
	 * 8: List
	 * 9: Preformatted plaintext
	 */
	char* data;
	char* meta;
};

void appendChild(struct docNode* target, struct docNode* newChild);
void printTree(struct docNode* node);
void eraseTree(struct docNode* node);

#endif
