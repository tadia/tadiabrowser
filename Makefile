objects = dom.o window.o network.o main.o url.o history.o
output = tb
CFLAGS = -Wall -O2 `pkg-config --cflags sdl2 cairo pangocairo libtls liburiparser`
LDFLAGS = -Wall -O2 -lpthread `pkg-config --libs --cflags sdl2 cairo pangocairo libtls liburiparser`

all: tadia

windows: CC = x86_64-w64-mingw32-gcc
windows: tadia

debug: CFLAGS += -g
debug: LDFLAGS += -g
debug: tadia

tadia: $(objects)
	cc -o $(output) $(objects) $(LDFLAGS)

dom.o: dom.h
window.o: window.h
network.o: network.h
url.o: url.h
history.o: history.h

.PHONY: clean fullclear run debugrun

clean:
	rm $(objects)
	
fullclean:
	rm $(output) $(objects)
	
run:
	./$(output)
	
debugrun:
	gdb $(output)
