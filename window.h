#ifndef TB_WINDOW_H
#define TB_WINDOW_H

#include <SDL2/SDL.h>
#include <pango/pango-layout.h>
#include <stdbool.h>
#include "network.h"

struct linkPos {
	int start;
	int end;
	int startx;
	int endx;
	char* addr;
};

struct docState {
	SDL_Renderer* renderer;
	int width;
	int height;
	int scroll;
	int docheight; // In cairo units
	struct tb_request_state* netstate;
	struct linkPos* links;
	int linkNum;
	struct SDL_Rect* destRect;
};

struct omniState {
	char* text;
	bool selected;
	int clickx;
	int clicky;
	int cursor; // Actual cursor postion
	int ocursor; // Old cursor position
	int vcursor; // Visual cursor position
};

int tb_window();

#endif
