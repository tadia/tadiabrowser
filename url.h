#ifndef TB_URL_H
#define TB_URL_H

#include "window.h"

struct redirectData {
	int port;
	char* hostname;
	char* url;
};

struct redirectData* tb_redirect_data(int currentPort, char* currentHostname, char* currentURL, char* uriString);

void tb_redirect(struct docState* state, struct omniState* ostate, char* addr);

#endif
